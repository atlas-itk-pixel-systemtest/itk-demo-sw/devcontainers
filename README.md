# devcontainers

## Overview

This repo constains base devcontainers to be used with DeMi development.

[Development containers](https://containers.dev/) are a thin layer on top of Docker and docker compose
to start containers from images and add a few additional features
like "Features" and "Templates".
The JSON manifest file lives with each repo:

```shell
<itk-demp-{repo}>/
  .devcontainer/
    devcontainer.json
    Dockerfile 
```

- `devcontainer.json` has a [specification](https://containers.dev/implementors/spec/).
- `Dockerfile` is optional and only needed if base image needs to be augmented (or created, as in this repo).

Devcontainers are supposed to provide a complete development environment.
They contain all development tools needed to work on a specific repository or multiple repositories
right within the container.

The best integration is currently with [VSCode](https://code.visualstudio.com/).
[See their documentation here](https://code.visualstudio.com/docs/devcontainers/containers).

The idea is to run VSCode as code-server in the browser and provide a complete development environment without any native tooling apart from Docker required at all.

Third-party tooling is still evolving, here are some relevant links:
  
- GitLab does not support devcontainers out-of-the-box. How to make it work: [Working with GitLab CI](https://containers.dev/guide/gitlab-ci)
- GitLab has a competitive technology called [GitPod](https://docs.gitlab.com/ee/integration/gitpod.html). Support tbd.
- CLI:
  - `devcontainer` [reference implementation](https://github.com/devcontainers/cli).
  - `odc` [open-devcontainer](https://gitlab.com/smoores/open-devcontainer) incomplete but provides nice features like [dotfiles](https://dotfiles.github.io/) loading and installing [code-server](https://coder.com/docs/code-server/latest).

## Base Containers

- `base-cc7`/ CERN Centos 7
- `base-alma9`/ Alma Linux 9

Redhat-flavored OS are not very well supported by the original devcontainers examples provided by Microsoft
(They are mainly focused on Debian / Ubuntu).
Some community contributed scripts exist but the aim is to provide taylored scripts for our principal OS.

The base containers are pretty much ready to go, but individual repos and of course individual developers
can add some customizations on top.

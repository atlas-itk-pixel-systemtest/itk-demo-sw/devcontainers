
FROM docker-cc7

# SHELL
# starship
RUN curl -fsSL https://starship.rs/install.sh -o install-starship.sh && sh install-starship.sh --yes
RUN touch /root/.bashrc && echo 'eval "$(starship init bash)"' >> /root/.bashrc

# direnv
RUN curl -sSL https://direnv.net/install.sh | bash
RUN echo 'eval "$(direnv hook bash)"' >> /root/.bashrc

# exa
# doesn't work on CC7?
# exa: /lib64/libc.so.6: version `GLIBC_2.18' not found (required by exa)
# RUN curl -fsSLO https://github.com/ogham/exa/releases/download/v0.10.0/exa-linux-x86_64-v0.10.0.zip
# RUN unzip exa-linux-x86_64-v0.10.0.zip -d exa
# RUN mv exa/bin/exa /usr/local/bin/
# RUN mv exa/man/exa.1 /usr/share/man/man1
# RUN mv /etc/bash_completion.d
# RUN mv completions/exa.bash /etc/bash_completion.d/

# lsd - to do - works on CC?

# PYTHON
RUN yum install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel db4-devel wget && yum clean all

RUN git clone --depth=1 https://github.com/pyenv/pyenv.git .pyenv
ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH
ENV eval "$(pyenv init -)"

ENV PYTHON_VERSION=3.9.12
RUN pyenv install ${PYTHON_VERSION}
RUN echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> .bashrc && \
  echo 'eval "$(pyenv init --path)"' >> .bashrc
# echo 'eval "$(pyenv virtualenv-init -)"' >> .bashrc
RUN pyenv global ${PYTHON_VERSION}

# Poetry
RUN curl -sSL -o get_poetry.py https://install.python-poetry.org && python3 get_poetry.py
ENV PATH $HOME/.local/bin:${PATH}
ENV PYTHON_KEYRING_BACKEND keyring.backends.null.Keyring
RUN poetry config virtualenvs.in-project true && \
  poetry config virtualenvs.prefer-active-python true

#pip + global python packages
RUN python3 -m pip install --upgrade pip && \
  python3 -m pip install python-gitlab

# NODE.JS: NVM, NPM
ENV NVM_DIR ${HOME}/.nvm
ENV NODE_VERSION 16.13.2

RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash && \
  echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"'  >> .bashrc && \
  echo '[ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"'  >> .bashrc && \
  . $NVM_DIR/nvm.sh && nvm install ${NODE_VERSION} && \
  nvm use ${NODE_VERSION}


#glab
# RUN curl -sSLO https://gitlab.com/gitlab-org/cli/-/releases/v1.28.1/downloads/glab_1.28.1_Linux_x86_64.rpm
# RUN yum -y install ./glab_1.28.1_Linux_x86_64.rpm
RUN curl -sSLO https://gitlab.com/gitlab-org/cli/-/releases/v1.28.1/downloads/glab_1.28.1_Linux_x86_64.tar.gz && \
  tar xvzf glab_1.28.1_Linux_x86_64.tar.gz && rm glab_1.28.1_Linux_x86_64.tar.gz && \
  mv bin/glab /usr/local/bin/



FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

RUN yum -y install dnf && yum clean all && \
  dnf -y update && dnf -y upgrade && \
  dnf -y groupinstall "Development Tools" && \
  dnf -y install \
  epel-release \
  which \
  sudo \
  tree \
  jq \
  # python
  perl-CPAN gettext-devel perl-devel openssl-devel zlib-devel \
  curl-devel expat-devel asciidoc xmlto docbook2X \
  gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel \
  openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel \ 
  db4-devel && \
  #  dnf -y remove git* && \
  dnf clean all


ENV HOME=/root
WORKDIR ${HOME}

# Git from Source
RUN ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi

# RUN dnf -y install wget curl
ENV GITVER="2.10.0"
#RUN wget https://github.com/git/git/archive/${GITVER}.tar.gz
RUN curl -fsSLO https://mirrors.edge.kernel.org/pub/software/scm/git/git-${GITVER}.tar.gz && \
  tar xvzf git-${GITVER}.tar.gz && \
  cd git-${GITVER} && \
  make configure && \
  ./configure --prefix=/usr && \
  make -j && \
  make install && \
  cd && rm -rf git-2.10.0*

# git config –global user,name “<name>”
# git config –global user.email “<email-address>”

# # Docker
# RUN curl -fsSL https://get.docker.com -o get-docker.sh && \
#   sh get-docker.sh


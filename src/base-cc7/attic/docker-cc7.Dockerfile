
FROM git-dnf-cc7

# Docker
# RUN curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh
RUN dnf remove docker \
  docker-client \
  docker-client-latest \
  docker-common \
  docker-latest \
  docker-latest-logrotate \
  docker-logrotate \
  docker-engine

RUN dnf -y install dnf-plugins-core && \
  dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo && \
  dnf install -y -q docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-ce-rootless-extras docker-buildx-plugin && \
  dnf clean all

# hadolint
RUN curl -fsSLo hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64 && \
  chmod +x hadolint &&\
  mv hadolint /usr/local/bin/hadolint
# RUN npm install -g @devcontainers/cli

# lazydocker
# https://github.com/jesseduffield/lazydocker
RUN curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash

# dive
# https://github.com/wagoodman/dive
RUN curl -OL https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.rpm && \
  rpm -i dive_0.9.2_linux_amd64.rpm && \
  rm dive_0.9.2_linux_amd64.rpm

# docker run --rm -it \
#   -v /var/run/docker.sock:/var/run/docker.sock \
#   -e DOCKER_API_VERSION=1.37 \
#   wagoodman/dive:latest <dive arguments...>
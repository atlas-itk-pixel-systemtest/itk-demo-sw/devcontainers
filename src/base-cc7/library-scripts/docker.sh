#!/usr/bin/env bash

# Docker
# RUN curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh
dnf -y remove docker \
  docker-client \
  docker-client-latest \
  docker-common \
  docker-latest \
  docker-latest-logrotate \
  docker-logrotate \
  docker-engine

dnf -y install dnf-plugins-core
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf install -y -q docker-ce \
docker-ce-cli \
containerd.io \
docker-compose-plugin \
docker-ce-rootless-extras \
docker-buildx-plugin
dnf clean all


# docker run --rm -it \
#   -v /var/run/docker.sock:/var/run/docker.sock \
#   -e DOCKER_API_VERSION=1.37 \
#   wagoodman/dive:latest <dive arguments...>
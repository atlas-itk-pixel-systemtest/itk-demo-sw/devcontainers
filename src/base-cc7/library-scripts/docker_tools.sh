#!/usr/bin/env bash

# hadolint
curl -fsSLo hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64
chmod +x hadolint
mv hadolint /usr/local/bin/hadolint
# RUN npm install -g @devcontainers/cli

# lazydocker
# https://github.com/jesseduffield/lazydocker
curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash

# dive
# https://github.com/wagoodman/dive
curl -OL https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.rpm
sudo rpm -i dive_0.9.2_linux_amd64.rpm
rm dive_0.9.2_linux_amd64.rpm

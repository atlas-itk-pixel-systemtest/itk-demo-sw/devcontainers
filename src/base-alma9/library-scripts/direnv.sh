#!/usr/bin/env bash

# direnv
curl -sSL https://direnv.net/install.sh | bash
echo 'eval "$(direnv hook bash)"' >> $HOME/.bashrc

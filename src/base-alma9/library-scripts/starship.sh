#!/usr/bin/env bash

curl -fsSL https://starship.rs/install.sh -o install-starship.sh
sh install-starship.sh --yes
touch $HOME/.bashrc
echo 'eval "$(starship init bash)"' >> $HOME/.bashrc



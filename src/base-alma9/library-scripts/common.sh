#!/usr/bin/env bash

yum -y install dnf
dnf -y update && dnf -y upgrade
dnf -y install epel-release centos-release-scl
dnf -y groupinstall "Development Tools"
dnf -y install \
which \
sudo \
tree \
jq \
perl-CPAN \
gettext-devel \
perl-devel \
openssl-devel \
zlib-devel \
curl-devel \
expat-devel \
asciidoc \
xmlto \
docbook2X \
gcc \
zlib-devel \
bzip2 \
bzip2-devel \
readline-devel \
sqlite \
sqlite-devel \
openssl-devel \
tk-devel \
libffi-devel \
xz-devel \
gdbm-devel \
ncurses-devel \
db4-devel

#  dnf -y remove git*
dnf clean all

#!/usr/bin/env bash

#glab
# RUN curl -sSLO https://gitlab.com/gitlab-org/cli/-/releases/v1.28.1/downloads/glab_1.28.1_Linux_x86_64.rpm
# RUN yum -y install ./glab_1.28.1_Linux_x86_64.rpm
curl -sSLO https://gitlab.com/gitlab-org/cli/-/releases/v1.28.1/downloads/glab_1.28.1_Linux_x86_64.tar.gz
tar xvzf glab_1.28.1_Linux_x86_64.tar.gz
rm glab_1.28.1_Linux_x86_64.tar.gz
sudo mv bin/glab /usr/local/bin/


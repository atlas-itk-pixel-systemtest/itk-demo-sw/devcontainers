#!/usr/bin/env bash

# Git from Source
ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi

GITVER="2.10.0"
curl -fsSLO https://mirrors.edge.kernel.org/pub/software/scm/git/git-${GITVER}.tar.gz
tar xvzf git-${GITVER}.tar.gz
cd git-${GITVER}
make configure
./configure --prefix=/usr
make -j
make install
cd
rm -rf git-${GITVER}*



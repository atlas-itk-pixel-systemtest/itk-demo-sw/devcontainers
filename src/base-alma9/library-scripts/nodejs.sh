#!/usr/bin/env bash

export NVM_DIR=${HOME}/.nvm
NODE_VERSION=16.13.2

curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"'  >> .bashrc
echo '[ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"'  >> .bashrc
. $NVM_DIR/nvm.sh && nvm install ${NODE_VERSION} && \
nvm use ${NODE_VERSION}
